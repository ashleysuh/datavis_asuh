// can add transparency or opacity
// possible to "fit" text to screen?

class ParallelCoord extends frame {
  Table data;
  int colCount;
  boolean hovered = false;
  
  public ParallelCoord( Table data, ArrayList<Integer> useColumns ){
    this.data = data;
    colCount = useColumns.size();
  }
  
  void draw(){    
    textFont( f, 16 );
    stroke(0);

    if( horizontal ){
      drawHorizontal();
    }
    else if ( vertical ){
      drawVertical();
    }
  }
  
  void drawHorizontal(){
    strokeWeight(1);
    
    // draw nodes
    for( int i = 0; i < colCount; i++ ){
      float vt = map( i, 0, colCount-1, v0-10, v0+h );
      line( u0, vt, w, vt );
      
      // draws axis tick marks
      for( int j = 0; j < 11; j++ )
        line( u0 + (j * ( (w-u0) / 10) ), vt-5, u0 + ( j * ( (w-u0) / 10 ) ), vt+5 );  
      
      // draw data points
      for( int k = 0; k < data.getRowCount(); k++ ){
        float dataPoint = data.getRow( k ).getFloat(i);
        float pos = map( dataPoint, min(data.getFloatColumn(i)), max(data.getFloatColumn(i)), u0, w );
        
        if( hover(pos, vt) ){
          stroke( 255,0,0 );
          fill( 255,0,0 );
          textFont( f, 14 );
          ellipse( pos, vt, 4, 4 );
          float x = map( dataPoint, u0, u0+w, u0, u0+w );
          text( "row value: " + x + ", column type: " + data.getColumnTitle(i),  width/2, 15 );
          stroke(0);
          fill(0);
        }
        else{
          stroke(0);
          fill(0);  
          ellipse( pos, vt, 4, 4 );
        }    
      }         
      drawLabels( true, vt, i );  
    }      
  }
  
  void drawVertical( ){
    strokeWeight(1);
    
    // draw edges
    for( int i = 0; i < data.getRowCount(); i++ ){
      for( int j = 0; j < colCount; j++ ){
        if( j < colCount - 1 ){ //up to 3
          // get the data in current column and next column
          float point1 = data.getRow( i ).getFloat( j );
          float point2 = data.getRow( i ).getFloat( j+1 );
          
          // map the two point's vertical positions to fit the screen
          float pos1 = map( point1, min(data.getFloatColumn(j)), max(data.getFloatColumn(j)), v0+h, v0 );
          float pos2 = map( point2, min(data.getFloatColumn(j+1)), max(data.getFloatColumn(j+1)), v0+h, v0 );
          
          // map their horizontal position 
          float hz1 = map( j, 0, colCount-1, u0, w );
          float hz2 = map( j+1, 0, colCount-1, u0, w );
          
          if( hovered ){
            //hovered = !hovered;
            stroke( 255,0,0 );
            fill( 255,0,0 );
            strokeWeight(0.7);
            line( hz1, pos1, hz2, pos2 );
          }
          else{
            stroke(0);
            strokeWeight(0.3);
            line( hz1, pos1, hz2, pos2 );
          }
        }
      }
    }
    
    // draw nodes
    for( int i = 0; i < colCount; i++ ){
      float hz = map( i, 0, colCount-1, u0, w );
      line( hz, v0, hz, h+v0 );
      
      // draws axis tick marks
      for( int j = 0; j < 11; j++ )
        line( hz-5, v0 + ( j * ( h / 10 ) ), hz+5, v0+(j*(h/10)) );
      
      // draw data points
      for( int k = 0; k < data.getRowCount(); k++ ){
        fill(0);
        stroke(0);
        float dataPoint = data.getRow( k ).getFloat( i );
        float pos = map( dataPoint, min(data.getFloatColumn(i)), max(data.getFloatColumn(i)), v0+h, v0 );
        
        if( hover(hz, pos) ){
          hovered = true;
          stroke( 255,0,0 );
          fill( 255,0,0 );
          textFont( f, 14 );
          ellipse( hz, pos, 4, 4 );
          float x = map( dataPoint, v0+h, v0, v0+h, v0 );
          text("row value: " + x + ", column type: " + data.getColumnTitle(i),  width/2, 15 );
          stroke(0);
          fill(0);
        }
        else{
          hovered = false;
          stroke(0);
          fill(0);  
          ellipse( hz, pos, 4, 4 );
        }
      }      
      drawLabels( false, hz, i );    
    }
  }
  
  void drawLabels( boolean horizontalView, float pos, int idx ){
    fill(0);
    stroke(0);
    if( horizontalView ){
      // labels are rotated by pi 
      float x_label1 = u0-4;
      float x_label2 = pos+5;
      pushMatrix();
      translate( x_label1,x_label2 );
      rotate( -HALF_PI );        
      textFont( f, 12 );
      text( min( data.getFloatColumn(idx)), 0, 0 );
      popMatrix();
      
      float y_label1 = w+4;
      float y_label2 = pos;
      pushMatrix();
      translate( y_label1,y_label2 );
      rotate( HALF_PI );
      textFont( headerFont, 14 );
      text( data.getColumnTitle(idx), 0, -15 );
      textFont( f, 12 );
      text( max(data.getFloatColumn(idx)), 0, 0 );
      popMatrix();
    }
    else{
      // draws header text for each column
      textFont( headerFont, 14 );
      text( data.getColumnTitle(idx), pos, v0-14 );
      
      // draws min,max on axis line
      textFont( f, 12 );
      text( min( data.getFloatColumn(idx)), pos, v0+h+15 );
      text( max(data.getFloatColumn(idx)), pos, v0-0.5 );
    }
  }
  
  boolean hover( float axis, float pos ){
    return ( mouseX >= axis-2 && mouseX <= axis+2 && mouseY >= pos-2 && mouseY <= pos+2 );
    /*{
      hovered = true;
      return true;
    }
    hovered = false;
    return false;*/
  }
}