/*
  Author: Ashley Suh
  Project #5: 2/27/2018
  CIS 4930: Data Visualization
  
  Uses references from processing.org,
  skeleton code provided by Paul Rosen
  
  datasets: srsatact.csv, iris.csv
*/

Table table; 
PFont f, headerFont;
boolean horizontal = false, vertical = false;

Display display = null;
frame PCPlot = null;

void setup(){
  size(800, 400);
  selectInput("Select an input file to view", "loadData");
  f = createFont("Arial", 16, true);
  headerFont = createFont("Arial Bold", 12);
  textAlign(CENTER, BOTTOM);
  display = new Display();
}

void draw(){
  background( 255 );
 
  if( display != null ){
    display.setPosition( 50, 60, width-50, height-50 );
    display.draw();
  }
  
  if( display != null && PCPlot != null ){
    PCPlot.setPosition(40, 55, width-40, height-80);
    PCPlot.draw();
  }
}

// calls the display screen if button is selected
void mousePressed(){
  display.selection();
}

abstract class frame {
  int u0, v0, w, h;
  void setPosition( int u0, int v0, int w, int h ){
    this.u0 = u0;
    this.v0 = v0;
    this.w = w;
    this.h = h;
  }
  abstract void draw();
  void mousePressed(){ }
}

void loadData(File selection) {
  if( selection == null ) {
    println("Window was closed or the user hit cancel.");
    selectInput("Select a file to process:", "fileSelected");
  } 
  else {
    println( "User selected " + selection.getAbsolutePath() );
    table = loadTable( selection.getAbsolutePath(), "header" );
    
    ArrayList<Integer> allColumns = new ArrayList<Integer>();
    for(int i = 0; i < table.getColumnCount(); i++){
      if( !Float.isNaN( table.getRow( 0 ).getFloat(i) ) ){
        allColumns.add(i);
        println("added column #" + i);
      }
      else{
        // add code here to do stuff with string columns
        println("not a float-type column");
      }
    }
    PCPlot = new ParallelCoord( table, allColumns );
  }
}