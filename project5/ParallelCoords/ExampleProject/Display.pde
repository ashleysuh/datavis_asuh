class Display {
  
  int u0, v0, w, h;
  color first = 255, second = 255, third = 255;
  
  void setPosition( int u0, int v0, int w, int h ){
    this.u0 = u0;
    this.v0 = v0;
    this.w = w;
    this.h = h;
  }
  
  void draw(){
    background(255);
      
    // draws buttons for selecting sketch1 or sketch2
    stroke(0);
    fill(first);
    rect(w-45, v0-60, 40, 20);
    fill(second);
    rect(w-5, v0-60, 40, 20);
    
    textFont(f, 20);
    if( !horizontal && !vertical ){
      noFill();
      rect(u0-20, v0-15, width-60, height-80);
      fill(0);
      textAlign(LEFT, CENTER);
      text("Parallel Coordinates Plot: LEGEND", u0, v0+10);
      text("View #1: Horizontal Axis Lines", u0, v0+50);
      text("View #2: Vertical Axis Lines", u0, v0+75);
 
      fill(255, 0, 0);
      text("Select any view from the top right corner.", u0, v0+110);
      text("Hover over a data point to view its specific information.", u0, v0+135);
    }
    
    fill(0);
    textFont(f, 12);
    textAlign(CENTER, BOTTOM);
    text("Swap Coordinates:", w-100, v0-42); 
    text("HORZ", w-24, v0-42);
    text("VERT", w+15, v0-42);
  }
  
  void selection(){
    // check if mouse is being pressed to select sketch
    if( mousePressed ){
      if( mouseX > w-45 && mouseX < w-5 && mouseY > v0-60 && mouseY < v0-35 ){
        horizontal = !horizontal;
        vertical = false;
      }
      else if( mouseX > w-5 && mouseX < w+35 && mouseY > v0-60 && mouseY < v0-35 ){
        vertical = !vertical;
        horizontal = false;
      }
    }
    
    // color buttons depending on which sketch is selected
    if( horizontal )
      first = color(230,240,255);
    else
      first = color(255);
      
    if( vertical )
      second = color(230,240,255);
    else
      second = color(255);
  }
}