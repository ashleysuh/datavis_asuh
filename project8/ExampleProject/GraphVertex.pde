// You shouldn't need to modify anything in this file but you can if you want

public static final float DAMPING_COEFFICIENT = 0.75f;


public class GraphVertex {

  /* extra credit variables */
  int num;                 // ID needed for distance matrix 
  float distanceTo;        // for dijkstra's
  boolean unvisited = true; // for dijkstra's
  GraphVertex prev = null; // for dijkstra's
  ArrayList<GraphVertex> neighbors = new ArrayList<GraphVertex>();
  
  /* provided variables */
  String id;
  PVector pos = new PVector(0,0);  
  PVector acc = new PVector(0,0);
  PVector vel = new PVector(0,0);
  PVector frc = new PVector(0,0);
  float mass = 5;
  float diam = 1;
  int group;
  boolean dragging = false;

  public GraphVertex( String _id, int _num, int _group, float _x, float _y ){
    id = _id;
    num = _num;
    group = _group;
    pos.set(_x,_y);
  }
  
  /* Extra credit */
  public void setNeighbors( ArrayList<GraphVertex> _neighbors ) { _neighbors = neighbors; }
  public ArrayList<GraphVertex> getNeighbors( ) { return neighbors; }

  /* Given functions */
  public String getID(){ return id; }
  
  public void    setPosition( float _x, float _y ){ pos.set(_x,_y); }
  public PVector getPosition(){ return pos; }

  public void    setMass( float m ){ mass = m; }
  public float   getMass( ){ return mass; }

  public void    setDiameter( float d ){ diam = d; }
  public float   getDiameter( ){ return diam; }

  public void    setVelocity( float _vx, float _vy ){ vel.set(_vx,_vy); }
  public PVector getVelocity(){ return vel; }
  
  public void    setAcceleration( float _ax, float _ay ){ acc.set(_ax,_ay); }
  public PVector getAcceleration(){ return acc; }

  public void    clearForce( ){ frc.set(0,0); }
  public void    addForce( float _fx, float _fy ){ frc.x+=_fx; frc.y+=_fy; }
  public PVector getForce(){ return frc; }
  
  // added for user clicking ability 
  boolean mouseInside() {
    return (pos.x-6)<mouseX && (pos.x+6)>mouseX && (pos.y-6)< mouseY && (pos.y+6)>mouseY;
  }
  
  // the following code probably shouldn't be modified unless you know what you're doing.
  void updatePosition( float deltaT ){
    
    float accelerationX = frc.x / mass;
    float accelerationY = frc.y / mass;
      
    setAcceleration(accelerationX, accelerationY);

    float velocityX = (vel.x + deltaT * accelerationX) * DAMPING_COEFFICIENT;
    float velocityY = (vel.y + deltaT * accelerationY) * DAMPING_COEFFICIENT;

    setVelocity(velocityX, velocityY);    
      
    float x = (float) (pos.x + deltaT * velocityX + accelerationX * Math.pow(deltaT, 2.0f) / 2.0f);
    float y = (float) (pos.y + deltaT * velocityY + accelerationY * Math.pow(deltaT, 2.0f) / 2.0f);

    x = constrain( x, 10, width-10 );
    y = constrain( y, 10, height-10 );
    
    setPosition( x, y );
  }  
}