// class for calculating the MDS projection
import java.util.Queue;

class MDS {
  
  ArrayList<GraphVertex> verts;
  ArrayList<GraphEdge> edges;
  
  float[][] lengthMatrix;
  float[][] distanceMatrix;
  
  MDS( ArrayList<GraphVertex> _verts, ArrayList<GraphEdge> _edges ){
    verts = _verts;
    edges = _edges;
    
    // set the matrices to be the size of # of nodes in graph 
    lengthMatrix = new float[verts.size()][verts.size()];
    distanceMatrix = new float[verts.size()][verts.size()];
    
    buildMatrix();
    //createDistanceMatrix();
  }
  
  public void buildMatrix(){
    for( GraphEdge e: edges ){
      int row = e.v0.num;
      int col = e.v1.num;
      
      lengthMatrix[row][col] = e.weight;
      lengthMatrix[col][row] = e.weight;
    }
  }
  
  public void createDistanceMatrix(){
    
    // going by the pseudocode on the wikipedia page for dijkstra's algorithm  
    for( GraphVertex source: verts ){
      // create vertex set Q
      Queue <GraphVertex> Q = new LinkedList<GraphVertex>();
      
      // create a tempory list of vertices to iterate over
      ArrayList<GraphVertex> graph = verts;
      
      // initialization
      for( GraphVertex v: graph ){
        // unknown distance from source
        v.distanceTo = Float.POSITIVE_INFINITY;  
        // previous node in optimal path from source
        v.prev = null;
        // all nodes initially in Q (unvisited)
        Q.add(v);
      }
      
      // distance from source to source
      source.distanceTo = 0;
      
      // iterate over graph until queue is empty
      while( !Q.isEmpty() ){
        
        GraphVertex u = null;
        float min = Float.POSITIVE_INFINITY;
        
        // find node with least distance to be selected first
        for( GraphVertex v: Q ){
          float temp = v.distanceTo;
          // update current min distance and vertex if temp < min
          if( temp < min ){
            min = temp;
            u = v;
          }
        }
        
        // remove selected node from queue
        Q.remove(u);
        
        // iterate over all the neighbors of u, create temp list
        ArrayList<GraphVertex> neighbors = u.getNeighbors();
        for( GraphVertex v: neighbors ){
          float alt = u.distanceTo + lengthMatrix[u.num][v.num];
          
          if( alt < v.distanceTo ){
            // a shorter path to v has been found
            v.distanceTo = alt;
            v.prev = u;
          }
        }
        
      }
    }
    
    
    /*
    Queue <GraphVertex> queue = new LinkedList<GraphVertex>();
    
    for( GraphVertex v: verts ){
      queue.add(v);      
    }
    
    while( !queue.isEmpty() ){
      // remove item we are traversing as the source
      GraphVertex source = queue.remove();
      
      // create a copy of the queue to iterate over 
      Queue<GraphVertex> copyQ = queue;
      
      while( !copyQ.isEmpty() ){
        // current target node
        GraphVertex target = copyQ.remove();
        float d = 0;
        
        // container for all adjacent neighbors
        HashMap<GraphVertex, Float> distances = new HashMap<GraphVertex, Float>();
        
        // put all neighbors of source node into hash map
        for( int i = 0; i < verts.size(); i++ ){
          if( originalMatrix[source.num][i] > 0 ){
            distances.put(verts.get(i), originalMatrix[source.num][i]);
          }
        }
        
        // iterate through neighbors to find shortest distance
        for( GraphVertex v: distances.keySet() ){
          //while( v != target ){
            
            
          //}
          
        }
        
        // add final shortest distance to distance matrix
        distanceMatrix[source.num][target.num] = d;
        distanceMatrix[target.num][source.num] = d;
      }
      
    }
    */
    
    // calculate the shortest distance
    /*for( int i = 0; i < verts.size(); i++ ){
      for( int j = i+1; j < verts.size()-1; i++ ){
        // find shortest path between vertex i and vertex j
        HashMap<GraphVertex, Float> queue = new HashMap<GraphVertex, Float>();
        // i is the source, j is the target
        
        
        
      }
    }*/
    
  }
  
}