/*
  SKELETON CODE PROVIDED BY: Paul Rosen
  Last Modified Author: Ashley Suh
  Project #8: 4/25/2018
  CIS 4930: Data Visualization

  README: 
    Project 8 is a force-directed layout using the Les Miserables co-occurence dataset.
    The layout was designed to be the most aesthetically similar to FDLs in d3.json
    
    1. Vertices are colored according to their group ID. The project allows up to 11 groups.
    2. Edges are thickened/darkened according to their weight.
    
*/

Frame myFrame = null;
MDS mds = null;

void setup() {
  size(800, 800);  
  smooth(8);
  
  selectInput("Select a file to process:", "fileSelected");
}

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
    selectInput("Select a file to process:", "fileSelected");
  } 
  else {
    println("User selected " + selection.getAbsolutePath());
    
    HashMap<String, GraphVertex> vertMap = new HashMap<String, GraphVertex>(); 
    ArrayList<GraphVertex>       verts = new ArrayList<GraphVertex>();
    ArrayList<GraphEdge>         edges = new ArrayList<GraphEdge>();

    // TODO: PUT CODE IN TO LOAD THE GRAPH    
    JSONObject data = loadJSONObject( selection );
    JSONArray nodes = data.getJSONArray("nodes");
    JSONArray links = data.getJSONArray("links");
    
    // load all vertices
    for( int i = 0; i < nodes.size(); i++ ){
      JSONObject current = nodes.getJSONObject(i);
      String id = current.getString("id");
      int group = current.getInt("group");
      
      GraphVertex v = new GraphVertex(id, i, group, random(15, width-15), random(15, height-15));
      
      // add key=id and value=vertex to hashmap for edge retrieval 
      vertMap.put( id, v );
      verts.add(v);
    }
    
    // load all edges
    for( int i = 0; i < links.size(); i++ ){
      JSONObject current = links.getJSONObject(i);
      String source = current.getString("source");
      String target = current.getString("target");
      float weight = current.getFloat("value");
      
      GraphVertex v0 = vertMap.get(source);
      GraphVertex v1 = vertMap.get(target);
      
      GraphEdge e = new GraphEdge( v0, v1, weight );
      edges.add(e);
    }
    
    /* EXTRA CREDIT: Put all adjecent nodes in a neighbors list for Dijkstra's algorithm */
    for( GraphEdge e: edges ){
      
      ArrayList<GraphVertex> n0 = e.v0.getNeighbors(); // grab current neighbor list
      ArrayList<GraphVertex> n1 = e.v1.getNeighbors(); 
      
      n0.add(e.v1); // add v1 to neighbors of v0
      n1.add(e.v0); // add v0 to neighbors of v1
      
      e.v0.setNeighbors(n0); // put the neighbor list back to update original
      e.v1.setNeighbors(n1);
    }

    myFrame = new ForceDirectedLayout( verts, edges );
    mds = new MDS( verts, edges );
  }
}


void draw() {
  background( 255 );
  
  if ( myFrame != null ) {
    myFrame.setPosition( 0, 0, width, height );
    myFrame.draw();
  }
}

void mousePressed() {
  myFrame.mousePressed();
}

void mouseReleased() {
  myFrame.mouseReleased();
}

abstract class Frame {

  int u0, v0, w, h;
  int clickBuffer = 2;
  void setPosition( int u0, int v0, int w, int h ) {
    this.u0 = u0;
    this.v0 = v0;
    this.w = w;
    this.h = h;
  }

  abstract void draw();
  
  void mousePressed() { }
  void mouseReleased() { }
  
}