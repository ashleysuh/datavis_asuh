// most modification should occur in this file
import java.util.*;

class ForceDirectedLayout extends Frame {
  
  /* THESE SHOULD NO LONGER BE ADJUSTED! */
  float RESTING_LENGTH = 20.0f;  
  float SPRING_SCALE   = 0.1f;   
  float REPULSE_SCALE  = 100.0f; 
  float TIME_STEP      = 0.65f; 

  // Storage for the graph
  ArrayList<GraphVertex> verts;
  ArrayList<GraphEdge> edges;
  
  // Keep track of min and max edge weight for drawing
  float minWeight = 0;
  float maxWeight = 0;
  
  // Hashmap for coloring the nodes (instead of hard coding color assignment, can read in 11 groups)
  HashSet<Integer> groups = new HashSet<Integer>();
  HashMap<Integer, Integer> colorMap = new HashMap<Integer, Integer>();
  color [] colors = {#a6cee3, #1f78b4, #b2df8a, #33a02c, #fb9a99, #e31a1c, 
                     #fdbf6f, #ff7f00, #cab2d6, #6a3d9a, #ffff99};
  
  ForceDirectedLayout( ArrayList<GraphVertex> _verts, ArrayList<GraphEdge> _edges ) {
    verts = _verts;
    edges = _edges;

    for( GraphVertex v: verts ){
      groups.add(v.group);
    }
    
    // assigns group IDs to a color value, up to 9 colors
    for( Integer i: groups ){
      //println(i);
      colorMap.put(i, colors[i]);
    }
    
    minWeight = Float.POSITIVE_INFINITY;
    maxWeight = Float.NEGATIVE_INFINITY;
    for( GraphEdge e: edges ){
      minWeight = min(e.weight, minWeight);
      maxWeight = max(e.weight, maxWeight);
    }
  }

  void applyRepulsiveForce( GraphVertex v0, GraphVertex v1 ) {
    
    // calculates euclidean distance, adds 0.0001 to avoid dividing by zero
    float d = v0.getPosition().dist( v1.getPosition() ) + 0.0001f;
    
    // calculate new vectors for each vertex (basically switching which one subtracts from which)
    PVector r1 = PVector.sub(v0.getPosition(), v1.getPosition());
    PVector r2 = PVector.sub(v1.getPosition(), v0.getPosition());

    // calculate the force for this distance 
    float f = ( REPULSE_SCALE * v0.getMass() * v1.getMass() ) / (d*d);
    
    // calculate the final repulsive force
    r1.mult(f);
    r1.mult(1/d);
    // r2 != r, because they need separate forces for each vertex
    r2.mult(f);
    r2.mult(1/d);
    
    // do something for forces within the same group vs. not in the same group
    if( v0.group == v1.group ){
      v0.addForce(r1.x / 1.25, r1.y / 1.25);
      v1.addForce(r2.x / 1.25, r2.y / 1.25);
    }
    else{
      v0.addForce(r1.x * 1.5, r1.y * 1.5);
      v1.addForce(r2.x * 1.5, r2.y * 1.5);      
    }
  }

  void applySpringForce( GraphEdge edge ) {
    // calculates euclidean distance, add 0.0001 to avoid dividing by zero
    float d = edge.v0.getPosition().dist(edge.v1.getPosition()) + 0.0001f;
    
    // initial calculation for attractive forces
    float force = SPRING_SCALE * max(0, d - RESTING_LENGTH);
    
    // calculate new vectors for each vertex (basically switching which one subtracts from which)
    PVector r1 = PVector.sub(edge.v1.getPosition(), edge.v0.getPosition());
    PVector r2 = PVector.sub(edge.v0.getPosition(), edge.v1.getPosition());
    
    // apply forces and magnitude to new vectors
    r1.mult(1/d);
    r1.mult(force);
    r2.mult(1/d);
    r2.mult(force);
    
    // do something for vertices in the same group vs. not in the same group
    if( edge.v0.group == edge.v1.group ){
      edge.v0.addForce(r1.x * 2, r1.y * 2);
      edge.v1.addForce(r2.x * 2, r2.y * 2);
    }
    else{
      edge.v0.addForce(r1.x / 1.5, r1.y / 1.5);
      edge.v1.addForce(r2.x / 1.5, r2.y / 1.5);      
    }  
  }

  void draw() {
    update();
    mousePressed();
    
    // sets the position before drawing if user has selected node
    for( GraphVertex v: verts ){
      if( v.dragging && mousePressed ){
        v.setPosition(constrain(mouseX, u0+10, w-10), constrain(mouseY, v0+10, h-10));
      }
    }
    
    // draw edges with respect to edge weight 
    for( GraphEdge e: edges ){      
      fill(0);
      
      // strokeVal is how dark the lines are colored, where darker edges are high-weight edges
      float strokeVal = map( e.weight, minWeight, maxWeight, 75, 150);
      stroke(105, 105, 105, strokeVal); // 4th parameter controls transparency of edges 
      
      // weightVal is how thick the lines are drawn, where thicker lines are high-weight edges
      float weightVal = map(e.weight, minWeight, maxWeight, 2, 6);
      strokeWeight(weightVal);      
      line(e.v0.getPosition().x, e.v0.getPosition().y, e.v1.getPosition().x, e.v1.getPosition().y);
    }
    
    // draw vertices for graph, shows node ID if being hovered over
    for( GraphVertex v: verts ){
      if( v.mouseInside() ){
        fill(0);
        textSize(14);
        text(v.id, v.pos.x+10, v.pos.y+10);
      }
            
      strokeWeight(1.5);
      stroke(255);
      fill( colorMap.get(v.group) );   
      ellipse( v.getPosition().x, v.getPosition().y, 13, 13 );
    }
    
    drawGroups();
  }
  
  void drawGroups(){
    // draw legend for each group id
    int buffer = 0;
    for( Integer i: groups ){
      fill( colorMap.get(i) );
      strokeWeight(0.5);
      stroke(0);
      ellipse( u0+w-75, v0+10+buffer, 12, 12 );
      fill(0);
      textSize(14);
      text("Group " + i, u0+w-65, v0+15+buffer);
      buffer += 15;
    }
  }

  void mousePressed() { 
    for( GraphVertex v: verts ){
      // check if the mouse is hovering over a node
      if( v.mouseInside() && mousePressed ){
        v.dragging = true;
      }
    }
  }

  void mouseReleased() {    
    for( GraphVertex v: verts ){
      v.dragging = false;
    }
  }

  // The following function applies forces to all of the nodes. 
  // This code does not need to be edited to complete this 
  // project (and I recommend against modifying it).
  void update() {
    for ( GraphVertex v : verts ) {
      v.clearForce();
    }

    for ( GraphVertex v0 : verts ) {
      for ( GraphVertex v1 : verts ) {
        if ( v0 != v1 ) applyRepulsiveForce( v0, v1 );
      }
    }

    for ( GraphEdge e : edges ) {
      applySpringForce( e );
    }

    for ( GraphVertex v : verts ) {
      v.updatePosition( TIME_STEP );
    }
  }
}