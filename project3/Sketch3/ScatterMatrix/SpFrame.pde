class SpFrame {
  FloatList xList, yList;
  boolean nonDiagonal = true;
  int iteration, u0, v0, w, h;  
  
  public SpFrame(){}
  
  void setDiagonal( boolean _d, int _i ){
    nonDiagonal = _d; 
    iteration = _i;
  }
  
  void setDimensions( int u0, int v0, int w, int h ){
    this.u0 = u0; 
    this.v0 = v0;
    this.w = w;
    this.h = h;
  }
  
  void setLists( FloatList _xList, FloatList _yList ){
    xList = _xList;
    yList = _yList;
  }
  
  void draw(){
    textFont(f, 18);
    stroke(153);
    strokeWeight(1);
    noFill();
    rect(u0, v0, w, h);
    
    // draws the scatterplot only if i != j during drawing for-loop
    if( nonDiagonal ){
      int numPoints = min(xList.size(), yList.size());
      
      // change the min/max by 10% so there is whitespace buffer in the grid
      float xRange = (xList.max() - xList.min()) / 10;
      float yRange = (yList.max() - yList.min()) / 10;
      float xMin = xList.min() - xRange;
      float xMax = xList.max() + xRange;
      float yMin = yList.min() - yRange;
      float yMax = yList.max() + yRange;
    
      // draws the data points    
      for( int i = 0; i < numPoints; i++ ){
        float x = map(xList.get(i), xMin, xMax, u0, w+u0);
        float y = map(yList.get(i), yMin, yMax, h+v0, v0);
        
        // map the x+y values so we can color it in a range
        float colorRange = map((xList.get(i) + yList.get(i)), (xMin + yMin), (xMax + yMax), 255, 0);
  
        stroke(0,0,255);
        strokeWeight(0);
        fill(255, colorRange, 100);
        ellipse(x, y, 5, 5);
      }
    }
    else{
      textFont(f, 24);
      fill(0);
      text(titles[iteration], u0+(w/2), v0+10+(h/2));
    }
  }
}