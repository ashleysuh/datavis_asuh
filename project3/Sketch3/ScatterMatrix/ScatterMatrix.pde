/*
  Author:     Ashley Suh
  Project #3: 2/6/2018
  CIS 4930:   Data Visualization
  
  Purpose:    Scatterplot Matrix for all columns in a 4x4 grid
              Works on additional datasets.
           
  References: processing.org; skeleton code provided by Paul Rosen
  
  README:     The dataset autoloaded is found in the directory where this program
              belongs, the folder called "data". The dataset is named "scatterplotScores.csv".
              To view a different file, a command prompt will allow user to load
              a different dataset from your directory. To choose another dataset,
              go to the directory this program was downloaded to, open
              "Sketch1" --> "ScatterPlot" --> "data" --> select "Iris.csv" 
              
              ** YOU ABSOLUTELY MUST CHOOSE A FILE TO LOAD OR CLOSE FILE PROMPT TO VIEW SKETCH ** */

// look up color brewer
Table table;
FloatList [] lists; 
String [] titles;
SpFrame [] myFrames;
PFont f;
boolean selected = false;

void setup(){
  size(800, 600);
  selectInput("SELECT A FILE TO VIEW, or close prompt to view default", "loadData");
  // initialize the 4x4 matrix
  myFrames = new SpFrame[16];
  for( int i = 0; i < 16; i++ ){
    SpFrame tempFrame = new SpFrame();
    myFrames[i] = tempFrame;
  }
  f = createFont("Arial", 16, true);
}

void draw(){
  background(255);
  fill(0);
    
  if( myFrames != null && selected){
    int u0 = 20;
    int v0 = 30;
    int w = width-40;
    int h = height-50;
   
    textFont(f,24);
    textAlign(CENTER,BOTTOM);
    text("Scatterplot Matrix", 375, 25);
    
    int k = 0;
    // creates a 4x4 scatterplot matrix
    for( int i = 0; i < 4; i++ ){
      for( int j = 0; j < 4; j++ ){
        SpFrame tempFrame = myFrames[k];
        tempFrame.setLists(lists[i], lists[j]);
        tempFrame.setDimensions(u0+(j*(w/4)), v0+(i*(h/4)), w/4, h/4);
        if( i == j ){
          myFrames[k].setDiagonal(false, i);
        }
        myFrames[k++].draw();
      }
    }
  }
}

void loadData(File selection) {
  String path;
  if( selection == null ) {
      println( "No selection chosen, so drawing is 'scatterplotScores.csv'. " );
      path = "scatterplotScores.csv";
      table = loadTable(path, "header");
      
      int colSize = 4;
      lists = new FloatList[colSize];
      for( int i = 0; i < colSize; i++ ){
        lists[i] = table.getFloatList(i);
      }
      titles = new String[table.getColumnCount()];
      titles = table.getColumnTitles();
      selected = true;
  } else {
      println( "User selected " + selection.getAbsolutePath() );
      path = selection.getAbsolutePath();
      table = loadTable(path, "header");
      
      int colSize = 4;
      lists = new FloatList[colSize];
      for( int i = 0; i < colSize; i++ ){
        lists[i] = table.getFloatList(i);
      }
      titles = new String[table.getColumnCount()];
      titles = table.getColumnTitles();
      selected = true;
  }
}