/*
  Author:     Ashley Suh
  Project #3: 2/6/2018
  CIS 4930:   Data Visualization
  
  Purpose:    Scatterplot of ACT scores versus GPA scores
              Works on additional datasets.
           
  References: processing.org; skeleton code provided by Paul Rosen
  
  README:     The dataset autoloaded is found in the directory where this program
              belongs, the folder called "data". The dataset is named "scatterplotScores.csv".
              To view a different file, a command prompt will allow user to load
              a different dataset from your directory. To choose another dataset,
              go to the directory this program was downloaded to, open
              "Sketch1" --> "ScatterPlot" --> "data" --> select "Iris.csv" 
              
              ** YOU ABSOLUTELY MUST CHOOSE A FILE TO LOAD OR CLOSE FILE PROMPT TO VIEW SKETCH ** */

Table table; // built-in data structure to hold data from csv file
FloatList xList, yList; // a list to hold row data
frame myFrame;
PFont f;
boolean selected = false;

void setup(){
  size(800, 600);
  selectInput("SELECT A FILE TO VIEW, or close prompt to view default", "loadData");
  myFrame = new SpFrame();
  f = createFont("Arial", 16, true);
  textAlign(CENTER,BOTTOM);
}

void draw(){
  background(255);
  fill(0);
  
  if( myFrame != null && selected){
    myFrame.setPosition(60, 30, width-80, height-70);
    myFrame.draw();    
  }
}

abstract class frame {
  
  int u0, v0, w, h;  
  void setPosition( int u0, int v0, int w, int h){
    this.u0 = u0; 
    this.v0 = v0;
    this.w = w;
    this.h = h;
  }

abstract void draw();

}

void loadData(File selection) {
  String path;
  if( selection == null ) {
      println( "No selection chosen, so drawing is 'scatterplotScores.csv'. " );
      path = "scatterplotScores.csv";
      table = loadTable(path, "header");
      xList = table.getFloatList(2);
      yList = table.getFloatList(3);
      
      selected = true;
  } else {
      println( "User selected " + selection.getAbsolutePath() );
      path = selection.getAbsolutePath();
      table = loadTable(path, "header");
      xList = table.getFloatList(2);
      yList = table.getFloatList(3);    
      
      selected = true;
  }
}