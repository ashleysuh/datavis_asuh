
class SpFrame extends frame{
  
  void draw(){
    textFont(f, 20);
    stroke(153);
    noFill();
    rect(u0, v0, w, h);
    
    // draws labels for graph 
    String [] titles = new String[table.getColumnCount()];
    titles = table.getColumnTitles();
    text("Scatterplot of " + titles[0] + " vs. " + titles[1], width/2, 20);
    textFont(f,13);
    
    // x-coordinate label
    text(titles[2], width/2, height-5 ); 
    
    // y-coordinate label (vertical text)
    float y_label1 = u0/2-10;
    float y_label2 = height/2;
    pushMatrix();
    translate(y_label1,y_label2);
    rotate(-HALF_PI);
    text(titles[3], 0, 0);
    popMatrix();
    
    // choose to draw the number of points that is the minimum between the two columns
    int numPoints = min(xList.size(), yList.size());
    
    // change the min/max by 10% so there is whitespace buffer in the grid
    float xRange = (xList.max() - xList.min()) / 10;
    float yRange = (yList.max() - yList.min()) / 10;
    float xMin = xList.min() - xRange;
    float xMax = xList.max() + xRange;
    float yMin = yList.min() - yRange;
    float yMax = yList.max() + yRange;

    for( int i = 0; i < 11; i++ ){
        textFont(f,11);
        stroke(153);
        // x-axis (horizontal line)
        line(u0+(i*(w/10)), v0, u0+(i*(w/10)), v0+h);
        // y-axis (vertical line)
        line(u0, v0+(i*(h/10)), u0+w, v0+(i*(h/10)) );
        
        // values to map x-y range tick marks
        float xValue = map(i, 0, 10, xMin, xMax);
        float yValue = map(i, 0, 10, yMax, yMin);
        
        // text is a little tricky to get aligned
        text(nf(yValue,0,2), u0/2+10, v0+4+(i*((h)/10)));
        text(nf(xValue,0,2), u0+5+(i*((w-5)/10)), v0+h+15);
    }
    
    // draws the data points    
    for( int i = 0; i < numPoints; i++ ){
      float x = map(xList.get(i), xMin, xMax, u0, w+u0);
      float y = map(yList.get(i), yMin, yMax, h+v0, v0);
      
      // map the x+y values so we can color it in a range
      float colorRange = map((xList.get(i) + yList.get(i)), (xMin + yMin), (xMax + yMax), 255, 0);

      stroke(0,0,255);
      strokeWeight(0);
      fill(255, colorRange, 100);
      ellipse(x, y, 10, 10);
      
    }
  }
}