
class Scatterplot extends frame {
   
  float minX, maxX;
  float minY, maxY;
  int idx0, idx1;
  int border = 40;
  boolean drawLabels = true;
  float spacer = 10;
  
   Scatterplot( Table data, int idx0, int idx1 ){
     
     this.idx0 = idx0;
     this.idx1 = idx1;
     
     minX = min(data.getFloatColumn(idx0));
     maxX = max(data.getFloatColumn(idx0));
     
     minY = min(data.getFloatColumn(idx1));
     maxY = max(data.getFloatColumn(idx1));
   }
   
   void draw(){
     //background(255);
     
     if( plot ){
       for( int i = 0; i < 11; i++ ){
         textFont(f,11);
         stroke(153);
         // x-axis (horizontal line)
         line(u0+(i*(w/10)), v0, u0+(i*(w/10)), v0+h);
         // y-axis (vertical line)
         line(u0, v0+(i*(h/10)), u0+w, v0+(i*(h/10)) );
        
         // values to map x-y range tick marks
         float xValue = map(i, 0, 10, minX, maxX);
         float yValue = map(i, 0, 10, maxY, minY);
        
         // text is a little tricky to get aligned
         text(nf(yValue,0,2), u0/2-10, v0+4+(i*((h)/10)));
         text(nf(xValue,0,2), u0-15+(i*((w-5)/10)), v0+h+15);
    }
     }
     
     for( int i = 0; i < table.getRowCount(); i++ ){
        TableRow r = table.getRow(i);
        
        float x = map( r.getFloat(idx0), minX, maxX, u0+spacer, u0+w-spacer );
        float y = map( r.getFloat(idx1), minY, maxY, v0+h-spacer, v0+spacer );
        float colorRange = map(r.getFloat(idx0) + r.getFloat(idx1), (minX + minY), (maxX + maxY), 255, 0);
        
        stroke(0,0,255);
        strokeWeight(0);
        fill(255, colorRange, 100);
        if( !plot )
          ellipse( x, y, 4, 4 );
        else
          ellipse( x, y, 10, 10 );
     }
     
     stroke(0);
     noFill();
     strokeWeight(0.5);
     if( !plot )
       rect( u0+border,v0+border, w-2*border, h-2*border);

     if( drawLabels ){
       fill(0);
       text(table.getColumnTitle(idx0), width/2, height-5 ); 
       
       text( table.getColumnTitle(idx0), u0+width/2, v0+height-10 );
       pushMatrix();
       translate( u0-45, height/2 );
       rotate( -PI/2 );
       text( table.getColumnTitle(idx1), 0, 0 );
       popMatrix();
     }
   }
  
}