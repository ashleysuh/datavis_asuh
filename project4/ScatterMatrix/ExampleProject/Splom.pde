

class Splom extends frame {
  
  ArrayList<Scatterplot> plots = new ArrayList<Scatterplot>( );
  int colCount;
  Table data;
  float border = 20;
  int [] indices = new int[2];
  boolean single = false;
    
  Splom( Table data, ArrayList<Integer> useColumns ){
    this.data = data;
    colCount = useColumns.size();
    for( int j = 0; j < colCount-1; j++ ){
      for( int i = j+1; i < colCount; i++ ){
        Scatterplot sp = new Scatterplot( table, useColumns.get(j), useColumns.get(i) );
        plots.add(sp);
      }
     }
  }
   
  void setPosition( int u0, int v0, int w, int h ){
    super.setPosition(u0,v0,w,h);

    int curPlot = 0;
    for( int j = 0; j < colCount-1; j++ ){
       for( int i = j+1; i < colCount; i++ ){
          Scatterplot sp = plots.get(curPlot++);
          int su0 = (int)map( i, 1, colCount, u0+border, u0+w-border );
          int sv0 = (int)map( j, 0, colCount-1, v0+border, v0+h-border );
          sp.setPosition( su0, sv0, (int)(w-2*border)/(colCount-1), (int)(h-2*border)/(colCount-1) );
          sp.drawLabels = false;
          sp.border = 3;
     }
    } 
  }
   
  void draw() {
    textFont(f,16);
    text("Scatterplot Matrix", 350, 20);
    
    if( !plot ){
      for( Scatterplot s : plots ){
        s.draw(); 
      }
    }
    else{
      Scatterplot SP = new Scatterplot( table, indices[0] , indices[1] );
      //background(255);
      SP.setPosition(60, 30, width-80, height-70);
      SP.draw();
    }
  }

  void mousePressed(){ 
    for( Scatterplot sp : plots ){
       if( sp.mouseInside() ){
         indices[0] = sp.idx0;
         indices[1] = sp.idx1;
         plot = !plot;
         
       }
       /*if( plot && !single ){
         if( mouseX < width && mouseX > 0 && mouseY < height && mouseY > 0 ){
           println("hello");//plot = !plot;
         }
       }*/
    }
  }
}