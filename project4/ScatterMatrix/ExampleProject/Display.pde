class Display {
  
  int u0, v0, w, h;
  color first = 255;//, second = 255, third = 255;
  
  void setPosition( int u0, int v0, int w, int h ){
    this.u0 = u0;
    this.v0 = v0;
    this.w = w;
    this.h = h;
  }
  
  void draw(){
    background(255);
      
    // draws buttons for selecting sketch1, sketch2, or sketch3
    stroke(0);
    fill(first);
    rect(w-85, v0-60, 40, 20);
    /*fill(second);
    rect(w-45, v0-60, 40, 20);
    fill(third);
    rect(w-5, v0-60, 40, 20);*/
    
    textFont(f, 20);
    if( !sketch1 ){
      noFill();
      rect(u0-20, v0-15, width-60, height-80);
      fill(0);
      text("LEGEND", u0-10, v0+10);
      text("View #1: ScatterMatrix", u0-10, v0+50);
      //text("View #2: Line Chart", u0-10, v0+75);
      //text("View #3: ScatterMatrix", u0-10, v0+100);
      
      fill(255, 0, 0);
      text("Select view #1 for the scatterplot matrix.", u0-10, v0+80);
      text("Click on a specific scatterplot to view the full graph.", u0-10, v0+100);
      text("Click again to return to full matrix view.", u0-10, v0+120);
    }
    
    fill(0);
    textFont(f, 14);
    text("Select View:", w-180, v0-45); 
    text("1", w-70, v0-45);
    //text("2", w-30, v0-45);
    //text("3", w+10, v0-45);
  }
  
  void selection(){
    // check if mouse is being pressed to select sketch
    if( mousePressed ){
      if( mouseX > w-85 && mouseX < w-45 && mouseY > v0-60 && mouseY < v0-35 ){
        sketch1 = !sketch1;
        //sketch2 = sketch3 = false;
      }
      /*else if( mouseX > w-45 && mouseX < w-5 && mouseY > v0-60 && mouseY < v0-35 ){
        sketch2 = !sketch2;
        sketch1 = sketch3 = false;
      }
      else if( mouseX > w-5 && mouseX < w+35 && mouseY > v0-60 && mouseY < v0-35 ){
        sketch3 = !sketch3;
        sketch1 = sketch2 = false;
      }*/
    }
    // color buttons depending on which sketch is selected
    if( sketch1 )
      first = color(220,220,220);
    else
      first = color(255);
      
    /*if( sketch2 )
      second = color(220,220,220);
    else
      second = color(255);
      
    if( sketch3 )
      third = color(220,220,220);
    else
      third = color(255);*/
  }
}