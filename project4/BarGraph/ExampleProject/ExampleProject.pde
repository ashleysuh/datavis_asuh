/*
  Author: Ashley Suh
  Project #4: 2/19/2018
  CIS 4930: Data Visualization
  
  Uses references from processing.org,
  skeleton code provided by Paul Rosen
  
  datasets: choose one of the 3 datasets in my project4's folder -
    srsatact.csv, dengue.csv, ketchup.csv
*/

Table table; 
PFont f;
boolean sketch1=false, sketch2=false, sketch3=false;

Display display = null;
frame BarChart = null;

void setup(){
  size(800, 600);
  selectInput("Select an input file to view", "loadData");
  f = createFont("Arial", 16, true);
  display = new Display();
}

void draw(){
  background( 255 );
 
  if( display != null ){
    display.setPosition( 50, 60, width-50, height-50 );
    display.draw();
  }
  
  if( display != null && sketch1 == true ){
    BarChart.setPosition(50, 40, width-30, height-80);
    BarChart.draw();
  }
  
  if( display != null && sketch2 == true ){
    BarChart.setPosition(50, 40, width-30, height-80);
    BarChart.draw();
  }
  
  if( display != null && sketch3 == true ){
    BarChart.setPosition(50, 40, width-30, height-80);
    BarChart.draw();
  }
}

// calls the display screen if button is selected
void mousePressed(){
  display.selection();
}

abstract class frame {
  int u0, v0, w, h;
  int clickBuffer = 2;
  void setPosition( int u0, int v0, int w, int h ){
    this.u0 = u0;
    this.v0 = v0;
    this.w = w;
    this.h = h;
  }
  abstract void draw();
  abstract void setIndices(int idx0, int idx1);
  
  void mousePressed(){ }
}

void loadData(File selection) {
  if( selection == null ) {
    println("Window was closed or the user hit cancel.");
    selectInput("Select a file to process:", "fileSelected");
  } 
  else {
    println( "User selected " + selection.getAbsolutePath() );
    table = loadTable( selection.getAbsolutePath(), "header" );
    
    ArrayList<Integer> allColumns = new ArrayList<Integer>();
    for(int i = 0; i < table.getColumnCount(); i++){
      if( !Float.isNaN( table.getRow( 0 ).getFloat(i) ) ){
        allColumns.add(i);
      }
      else{
        //println( i + " - type string" );
      }
    }
    BarChart = new BarChart();
  }
}