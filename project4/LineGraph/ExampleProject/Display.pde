class Display {
  
  int u0, v0, w, h;
  color first = 255, second = 255, third = 255;
  
  void setPosition( int u0, int v0, int w, int h ){
    this.u0 = u0;
    this.v0 = v0;
    this.w = w;
    this.h = h;
  }
  
  void draw(){
    background(255);
      
    // draws buttons for selecting sketch1, sketch2, or sketch3
    stroke(0);
    fill(first);
    rect(w-85, v0-60, 40, 20);
    fill(second);
    rect(w-45, v0-60, 40, 20);
    fill(third);
    rect(w-5, v0-60, 40, 20);
    
    textFont(f, 20);
    if( (!sketch1) && (!sketch2) && (!sketch3) ){
      noFill();
      rect(u0-20, v0-15, width-60, height-80);
      fill(0);
      text("Line Chart: LEGEND", u0-10, v0+10);
      text("View #1: Column 0 vs. Column 1", u0-10, v0+50);
      text("View #2: Column 0 vs. Column 2", u0-10, v0+75);
      text("View #3: Column 1 vs. Column 2", u0-10, v0+100);
      
      fill(255, 0, 0);
      text("Select any view from the top right corner.", u0-10, v0+130);
      text("Hover over a vertex to view the data for that node.", u0-10, v0+155);
    }
    
    fill(0);
    textFont(f, 14);
    text("Select View:", w-180, v0-45); 
    text("1", w-70, v0-45);
    text("2", w-30, v0-45);
    text("3", w+10, v0-45);
  }
  
  void selection(){
    // check if mouse is being pressed to select sketch
    if( mousePressed ){
      if( mouseX > w-85 && mouseX < w-45 && mouseY > v0-60 && mouseY < v0-35 ){
        sketch1 = !sketch1;
        sketch2 = sketch3 = false;
        table.sort(0);
        LineChart.setIndices(0, 1);
      }
      else if( mouseX > w-45 && mouseX < w-5 && mouseY > v0-60 && mouseY < v0-35 ){
        sketch2 = !sketch2;
        sketch1 = sketch3 = false;
        table.sort(0);
        LineChart.setIndices(0,2);
      }
      else if( mouseX > w-5 && mouseX < w+35 && mouseY > v0-60 && mouseY < v0-35 ){
        sketch3 = !sketch3;
        sketch1 = sketch2 = false;
        table.sort(1);
        LineChart.setIndices(1,2);
      }
    }
    // color buttons depending on which sketch is selected
    if( sketch1 )
      first = color(240,248,255);
    else
      first = color(255);
      
    if( sketch2 )
      second = color(240,248,255);
    else
      second = color(255);
      
    if( sketch3 )
      third = color(240,248,255);
    else
      third = color(255);
  }
}