
public class BarChart extends frame {

  public BarChart( ){}
  
  public void setYAxis( int _y ){
    counterY = _y;
  }
  
  void draw(){  
    yList = table.getFloatList(counterY);
    
    textFont(f, 14);
    strokeWeight(1.2);
    stroke(0);
    fill(0);    
    
    text(table.getColumnTitle(counterY) + " Graph", (w-u0)/2, v0+5);
    
    strokeWeight(1);
    noFill();
    stroke(0);
    line( u0-1, v0, u0-1, h+v0); // y-axis line
    line( u0, h+v0+1, w, h+v0+1 ); // x-axis line
    
    for( int i = 0; i < 11; i++ ){    
      stroke(0);
      textFont(f,9);
      float xTick = map(i, 0, 10, u0, w);

      if( i == 10 ){
        stroke(0,0,0,50);
        line(xTick, v0+h, xTick, v0);
      }
      else
        line(xTick, v0+h-5, xTick, v0+h+3);
      
      // y-axis (horizontal lines)
      stroke(0);
      float yTick = map(i, 0, 10, v0+h, v0);
      line(u0-1, yTick, u0+3, yTick );
      
      // values to map x-y range tick marks
      float xValue = map(i, 0, 10, 0, table.getRowCount());
      float yValue = map(i, 0, 10, yList.min(), yList.max());
      
      // text for axes
      text(String.format("%.1f",yValue), u0-13, yTick+5);
      if( i == 10 )
        textAlign(RIGHT, BOTTOM);
      else if( i == 0 )
        textAlign(LEFT, BOTTOM);
      text(String.format("%.0f",xValue), xTick, v0+h+15);
      textAlign(CENTER, BOTTOM);
    }
    
    for( int i = 0; i < table.getRowCount(); i++ ){
      strokeWeight(1);
      // map positions from u0->w
      float xPos = map(i, 0, table.getRowCount(), u0, w);
      float yPos = map(yList.get(i), yList.min(), yList.max(), v0+h, v0); 
      float rw = w/table.getRowCount();
      float colorRange = map(yList.get(i), yList.min(), yList.max(), 255, 0);
      fill(100, colorRange, 255);
      
      // check if hovering over a bar, then highlight for everything
      if( hoveredValue== i || (mouseX >= xPos && mouseX <= xPos+rw)){
        if( hoveredValue== i || (mouseY >= yPos && mouseY <= v0+h) ){
          stroke(0);
          strokeWeight(2);
          hoveredValue = i;
          fill(100, colorRange, 255);
          rect( xPos, yPos, rw, h+v0-yPos);
        }
      }
      else{
        strokeWeight(1);
        fill(100, colorRange, 255);
        stroke(100, colorRange, 255); 
        rect( xPos, yPos, rw, h+v0-yPos);
      }
      strokeWeight(1);
    }
  }
  
  void mousePressed(){}
}