
class Scatterplot extends frame {
   
  float minX, maxX;
  float minY, maxY;
  int idx0, idx1;
  int border = 10;
  boolean drawLabels = true;
  float spacer = 10;
  FloatList colorRanges = new FloatList();
  FloatList ranges = new FloatList();
  
   Scatterplot( Table data, int idx0, int idx1 ){
     
     this.idx0 = idx0;
     this.idx1 = idx1;
     
     minX = min(data.getFloatColumn(idx0));
     maxX = max(data.getFloatColumn(idx0));
     
     minY = min(data.getFloatColumn(idx1));
     maxY = max(data.getFloatColumn(idx1));
   }
   
   void draw(){
     //background(255);
     
     if( plot ){
       for( int i = 0; i < 11; i++ ){
         textFont(f,8);
         stroke(153);
         strokeWeight(0.5);
         // x-axis (horizontal line)
         line(u0+(i*(w/10)), v0, u0+(i*(w/10)), v0+h);
         
         // y-axis (vertical line)
         line(u0, v0+(i*(h/10)), u0+w-5, v0+(i*(h/10)) );
        
         // values to map x-y range tick marks
         float xValue = map(i, 0, 10, minX, maxX);
         float yValue = map(i, 0, 10, maxY, minY);
         fill(0);
         // text is a little tricky to get aligned
         text(String.format("%.2f",yValue), u0-14, v0+7+(i*((h)/10)));
         text(String.format("%.2f",xValue), u0-3+(i*((w-5)/10)), v0+h+15);
       }
     }
     
     for( int i = 0; i < table.getRowCount(); i++ ){
        TableRow r = table.getRow(i);
        
        float x = map( r.getFloat(idx0), minX, maxX, u0+spacer, u0+w-spacer );
        float y = map( r.getFloat(idx1), minY, maxY, v0+h-spacer, v0+spacer );
        float colorRange = map(r.getFloat(idx0) + r.getFloat(idx1), (minX + minY), (maxX + maxY), 255, 0);
        colorRanges.append(colorRange);
        ranges.append(r.getFloat(idx0) + r.getFloat(idx1));
        
        stroke(0);
        strokeWeight(0);
        if( plot ){
          if( mouseX >= x-9 && mouseX <= x+9 && mouseY >= y-9 && mouseY <= y+9 ){
            hoveredValue = i;
          }
        }
        if( hoveredValue == i ){
          strokeWeight(3);
        }
        else{
          strokeWeight(0);
        }
        fill(100, colorRange, 255);
        if( !plot )
          ellipse( x, y, 5, 5 );
        else
          ellipse( x, y, 10, 10 );
     }
     
     if( plot ){
       colorRanges.clear();
       ranges.clear();
       for( int i = 0; i < table.getRowCount(); i++ ){
          TableRow r = table.getRow(i);
          float colorRange = map(r.getFloat(idx0) + r.getFloat(idx1), (minX + minY), (maxX + maxY), 255, 0);
          colorRanges.append(colorRange);
          ranges.append(r.getFloat(idx0) + r.getFloat(idx1));
       }
       colorRanges.sort();
       ranges.sort();
       // create legend for scatterplot
       textFont(f, 12);
       fill(0);
       text(ranges.get(ranges.size() - 1), 1140, 120);
       stroke(100, colorRanges.get(0), 255);
       fill(100, colorRanges.get(0), 255);
       rect(1170, 100, 20, 25);
       
       fill(0);
       text(ranges.get(2*(ranges.size()-1)/3), 1140, 145);
       stroke(100, colorRanges.get((colorRanges.size()-1)/4), 255);
       fill(100, colorRanges.get((colorRanges.size()-1)/4), 255);
       rect(1170, 125, 20, 25);
       
       fill(0);
       text(ranges.get((ranges.size()-1)/2), 1140, 170);
       stroke(100, colorRanges.get((colorRanges.size()-1)/2), 255);
       fill(100, colorRanges.get((colorRanges.size()-1)/2), 255);
       rect(1170, 150, 20, 25);
  
       fill(0);
       text(ranges.get((ranges.size()-1)/4), 1140, 195);
       stroke(100, colorRanges.get(2*(colorRanges.size()-1)/3), 255);
       fill(100, colorRanges.get(2*(colorRanges.size()-1)/3), 255);
       rect(1170, 175, 20, 25);
       
       fill(0);
       text(ranges.get(0), 1140, 220);
       stroke(100, colorRanges.get(colorRanges.size() - 1));
       fill(100, colorRanges.get(colorRanges.size() - 1), 255);
       rect(1170, 200, 20, 25);
       
       noFill();
       stroke(0);
       strokeWeight(1);
       rect(1170, 100, 20, 125);
       rect(1110, 75, 85, 155);
       
       textFont(fb, 12);
       fill(0);
       text("(x+y) range", 1155, 93);
    }
     
     stroke(0);
     noFill();
     strokeWeight(0.5);
     if( !plot ){
       strokeWeight(1);
       if( mouseInside() ){
         textFont(f, 16);
         fill(0);
         textAlign(RIGHT, BOTTOM);
         text( table.getColumnTitle(idx0) + " & " + table.getColumnTitle(idx1), u0+w-5, v0+h-2);
         textAlign(CENTER,BOTTOM);
         strokeWeight(2);
         noFill();
       }
       else
         strokeWeight(1);
       rect( u0+border,v0+border, w-2*border, h-2*border);       
     }
   }
  
  void mousePressed(){}  
}