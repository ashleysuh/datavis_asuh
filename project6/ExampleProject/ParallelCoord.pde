class ParallelCoord extends frame {
  Table data;
  int colCount;
  boolean horizontal = true;
  
  public ParallelCoord( Table data, ArrayList<Integer> useColumns ){
    this.data = data;
    colCount = useColumns.size();
  }
  
  void draw(){  
    mousePressed();
    textFont( f, 12 );
    stroke(0);

    strokeWeight(1);
    for( int i = 0; i < colCount; i++ ){
      float vt = map( i, 0, colCount-1, v0-10, v0+h );
      strokeWeight(2);
      
      if( mouseX >= u0 && mouseX <= u0+w && mouseY >= vt-10 && mouseY <= vt+10){
        strokeWeight(5);
        stroke(255,0,0);
        line( u0, vt, w, vt );
        if( mousePressed ){
          counterY = i;
        }
      }
      else{
        stroke(0);
        strokeWeight(1.5);
        line( u0, vt, w, vt );
      }
      
      // draws axis tick marks
      for( int j = 0; j < 11; j++ ){
        strokeWeight(1);
        line( u0 + (j * ( (w-u0) / 10) ), vt-5, u0 + ( j * ( (w-u0) / 10 ) ), vt+5 );  
      }
      
      // draw data points
      for( int k = 0; k < data.getRowCount(); k++ ){
        float dataPoint = data.getRow( k ).getFloat(i);
        float pos = map( dataPoint, min(data.getFloatColumn(i)), max(data.getFloatColumn(i)), u0, w );
        
        if( i < colCount - 1 ){
          float dataPoint2 = data.getRow( k ).getFloat( i+1 );
          float pos2 = map( dataPoint2, min(data.getFloatColumn(i+1)), max(data.getFloatColumn(i+1)), u0, w);
          float vt2 = map( i+1, 0, colCount-1, v0-10, v0+h );
          strokeWeight(1);
          stroke(0, 0, 0, 75);
          
          if( k == hoveredValue ){
            fill(0);
            stroke(0);
            strokeWeight(3);
            line( pos, vt, pos2, vt2 );
          }
          else{
            fill(0);
            stroke(0, 0, 0, 65);
            strokeWeight(1);
            line( pos, vt, pos2, vt2 );
          }
          fill(0);
          stroke(0, 0, 0, 65);
          strokeWeight(1);
        }
      } 
      drawLabels( vt, i );  
    }
  }
  
  void drawLabels( float pos, int idx ){
    fill(0);
    stroke(0);

    // labels are rotated by pi 
    float x_label1 = u0-4;
    float x_label2 = pos+5;
    pushMatrix();
    translate( x_label1,x_label2 );
    rotate( -HALF_PI );        
    textFont( f, 10 );
    text( String.format("%.2f",min( data.getFloatColumn(idx))), 0, 0 );
    popMatrix();
    
    float y_label1 = w+4;
    float y_label2 = pos;
    pushMatrix();
    translate( y_label1,y_label2 );
    rotate( HALF_PI );
    textFont( headerFont, 10 );
    textAlign(CENTER);
    if( idx == 0 ){
      textAlign(LEFT);
    }
    else if( idx == table.getColumnCount() - 1){
      textAlign(RIGHT);
    }
    text( data.getColumnTitle(idx), 0, -15 );
    textAlign(CENTER);
    textFont( f, 10 );
    text( String.format("%.2f",max(data.getFloatColumn(idx))), 0, 0 );
    popMatrix();

  }
  
  boolean hover( float axis, float pos ){
    return ( mouseX >= axis-2 && mouseX <= axis+2 && mouseY >= pos-2 && mouseY <= pos+2 );
  }
  void mousePressed(){
  }
}