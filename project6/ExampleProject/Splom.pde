

class Splom extends frame {
  
  ArrayList<Scatterplot> plots = new ArrayList<Scatterplot>( );
  FloatList xList, yList;
  int colCount;
  float border = 5;
  int [] indices = new int[2];
    
  Splom( ArrayList<Integer> useColumns ){
    colCount = useColumns.size();
    for( int j = 0; j < colCount-1; j++ ){
      for( int i = j+1; i < colCount; i++ ){
        Scatterplot sp = new Scatterplot( table, useColumns.get(j), useColumns.get(i) );
        plots.add(sp);
      }
    }

    indices[0]=0;
    indices[1]=1;
  }
   
  void setPosition( int u0, int v0, int w, int h ){
    super.setPosition(u0,v0,w,h);

    int curPlot = 0;
    for( int j = 0; j < colCount-1; j++ ){
       for( int i = j+1; i < colCount; i++ ){
          Scatterplot sp = plots.get(curPlot++);
          int su0 = (int)map( j, 0, colCount-1, u0+border, u0+w-border );
          int sv0 = (int)map( i, 1, colCount, v0+border, v0+h-border );
          
          sp.setPosition( su0, sv0, (int)(w-2*border)/(colCount-1), (int)(h-2*border)/(colCount-1) );
          sp.drawLabels = false;
          sp.border = 3;
     }
    }    
  }
   
  void draw() {  
    textFont(f, 13);
    text("Click on a plot from the matrix to view it zoomed below.", u0+(2*w/3), v0+27);
    text("Hover over a data point from a graph to highlight.", u0+(2*w/3), v0+45);
    text("Select an axis from PCP to swap all axes.", u0+(2*w/3), v0+65);
    for( Scatterplot s : plots ){
      plot = false;
      s.draw(); 
      plot = true;
    } 
    
    Scatterplot SP = new Scatterplot( table, indices[0] , indices[1] );
    SP.drawLabels = true;
    SP.setPosition(730, 405, 465, 380);
    SP.draw();
  }

  void mousePressed(){ 
    for( Scatterplot sp : plots ){
       if( sp.mouseInside() ){
           indices[0] = sp.idx0;
           indices[1] = sp.idx1;
       }    
    }
  }
}