/*
  Author: Ashley Suh
  Project #6: 3/28/2018
  CIS 4930: Data Visualization

  README: Instructions are found on the top right corner of the dashboard.
  However, if these are confusing, here are the interactions:
  
  1. You can click on one of the axes in the parallel coordinates plot and it will
     update the line chart and bar chart to the correct axis.
  2. You can hover over a point in the bar chart or line chart and it will highlight that
     data point for all other charts in the left column. That interaction hasn't been
     fixed yet with hovering over parallel coordinates.
  3. Click on a scatterplot matrix to see a zoomed in version in the bottom right corner.
     Every click will also update the legend correctly.
  4. Hovering over a scatterplot matrix will show the axes (x-axis & y-axis, respectively).
  
  Datasets to use: srsatact.csv, iris.csv
*/

Table table; 
PFont f, fb, headerFont;
int counterY = 1;
FloatList yList;
int clickBuffer = 2;
boolean plot = false;
int hoveredValue = -1;

frame PCPlot = null;
frame BC = null;
frame LC = null;
frame ScatterMatrix = null;
frame ScatterPlot = null;

void setup(){
  size(1200, 800);
  smooth(8);
  selectInput("Select an input file to view", "loadData");
  f = createFont("Arial", 16, true);
  fb = createFont("Arial Bold", 14, true);
  headerFont = createFont("Arial Bold", 12);
  textAlign(CENTER, BOTTOM);
}

void draw(){
  background( 255 );
  //mousePressed();
  
  if( PCPlot != null ){
    PCPlot.setPosition(17, 1600/3+30, 670, 800/3-45 );
    PCPlot.draw();
  }
  
  if( BC != null ){
    BC.setPosition(25, 10, 690, 800/3 - 25);
    BC.draw();    
  }
  
  if( LC != null ){
    LC.setPosition(25, 800/3 + 25, 690, 800/3-38 );  
    LC.draw();
  }
  
  if( ScatterMatrix != null ){
    ScatterMatrix.setPosition(700, 0, 500, 400);
    ScatterMatrix.draw();
  }
  
  // draws borders
  line(0, 800/3, 700, 800/3);
  line(0, 800-800/3, 700, 800-800/3);
  line(700, 400, 1200, 400); 
  strokeWeight(2);
  line(700, 0, 700, 800);
  strokeWeight(1);
}

abstract class frame {
  int u0, v0, w, h;
  void setPosition( int u0, int v0, int w, int h ){
    this.u0 = u0;
    this.v0 = v0;
    this.w = w;
    this.h = h;
  }
  abstract void draw();
  abstract void mousePressed();
  
  boolean mouseInside(){
    return (u0-clickBuffer < mouseX) && (u0+w+clickBuffer)>mouseX && (v0-clickBuffer)< mouseY && (v0+h+clickBuffer)>mouseY; 
  }
}

void mousePressed(){
  ScatterMatrix.mousePressed();  
  BC.mousePressed();
}

void loadData(File selection) {
  if( selection == null ) {
    println("Window was closed or the user hit cancel.");
    selectInput("Select a file to process:", "fileSelected");
  } 
  else {
    println( "User selected " + selection.getAbsolutePath() );
    table = loadTable( selection.getAbsolutePath(), "header" );
    
    ArrayList<Integer> allColumns = new ArrayList<Integer>();
    for(int i = 0; i < table.getColumnCount(); i++){
      if( !Float.isNaN( table.getRow( 0 ).getFloat(i) ) ){
        allColumns.add(i);
      }
      else{
      }
    }
    BC = new BarChart();
    LC = new LineChart();
    PCPlot = new ParallelCoord( table, allColumns );
    ScatterMatrix = new Splom( allColumns );
    ScatterPlot = new Scatterplot( table, 0, 1 );
  }
}