  README: Instructions are found on the top right corner of the dashboard.
  However, if these are confusing, here are the interactions:
  
  1. You can click on one of the axes in the parallel coordinates plot and it will
     update the line chart and bar chart to the correct axis.
  2. You can hover over a point in the bar chart or line chart and it will highlight that
     data point for all other charts in the left column. That interaction hasn't been
     fixed yet with hovering over parallel coordinates.
  3. Click on a scatterplot matrix to see a zoomed in version in the bottom right corner.
     Every click will also update the legend correctly.
  4. Hovering over a scatterplot matrix will show the axes (x-axis & y-axis, respectively).
  
  Datasets to use: srsatact.csv, iris.csv