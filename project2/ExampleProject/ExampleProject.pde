/*
  Author: Ashley Suh
  Project #2: 1/29/2018
  CIS 4930: Data Visualization
  
  Uses references from processing.org,
  skeleton code provided by Paul Rosen
*/

Table table; 
PFont f;
boolean sketch1=false, sketch2=false, sketch3 = false;
boolean plot = false;

Display display = null;
frame BarChart = null;
frame LineChart = null;
frame ScatterMatrix = null;
frame ScatterPlot = null;

void setup(){
  size(800, 600);
  selectInput("SELECT A FILE TO VIEW, or close prompt to view default", "loadData");
  f = createFont("Arial", 16, true);
  display = new Display();
}

void draw(){
  background( 255 );
 
  if( display != null ){
    display.setPosition( 50, 60, width-50, height-50 );
    display.draw();
  }
  
  if( BarChart != null && sketch1 == true ){
    BarChart.setPosition(50, 40, width-30, height-80);
    BarChart.draw();
  }
  
  if( LineChart != null && sketch2 == true ){
    LineChart.setPosition(50, 40, width-30, height-80);
    LineChart.draw();
  }
  
  if( ScatterMatrix != null && sketch3 == true ){
    ScatterMatrix.setPosition(20, 30, width-30, height-70);
    ScatterMatrix.draw();
  }
}

// calls the display screen if button is selected
void mousePressed(){
  display.selection();
  ScatterMatrix.mousePressed();
}

abstract class frame {
  int u0, v0, w, h;
  int clickBuffer = 2;
  void setPosition( int u0, int v0, int w, int h ){
    this.u0 = u0;
    this.v0 = v0;
    this.w = w;
    this.h = h;
  }
  abstract void draw();
  
  void mousePressed(){ }
  
  boolean mouseInside(){
    return (u0-clickBuffer < mouseX) && (u0+w+clickBuffer)>mouseX && (v0-clickBuffer)< mouseY && (v0+h+clickBuffer)>mouseY; 
  }
}

void loadData(File selection) {
  if( selection == null ) {
    println("Window was closed or the user hit cancel.");
    selectInput("Select a file to process:", "fileSelected");
  } 
  else {
    println( "User selected " + selection.getAbsolutePath() );
    table = loadTable( selection.getAbsolutePath(), "header" );
    
    ArrayList<Integer> allColumns = new ArrayList<Integer>();
    for(int i = 0; i < table.getColumnCount(); i++){
      if( !Float.isNaN( table.getRow( 0 ).getFloat(i) ) ){
        allColumns.add(i);
      }
      else{
        //println( i + " - type string" );
      }
    }
    
    BarChart = new BarChart( table, allColumns.get(0), allColumns.get(1) );
    LineChart = new LineChart( table, allColumns.get(0), allColumns.get(1) );
    ScatterMatrix = new Splom( table, allColumns );
    ScatterPlot = new Scatterplot( table, allColumns.get(0), allColumns.get(1) );
  }
}