//PFont f;

class BarChart extends frame {
  float xMin, xMax;
  float yMin, yMax;
  int idx0, idx1;
  FloatList xList, yList;
    
  public BarChart( Table data, int idx0, int idx1 ){
    this.idx0 = idx0;
    this.idx1 = idx1;
    
    data.sort(idx0); // sort the x-values
    xList = table.getFloatList(idx0);
    yList = table.getFloatList(idx1);
    
    xMin = xList.min();
    xMax = xList.max();
    yMin = yList.min();
    yMax = yList.max();
  }
  
  void draw(){  
    textFont(f, 16);
    // graph title header
    text(table.getColumnTitle(idx0) + " vs. " + table.getColumnTitle(idx1), width/2-u0, 20);
    
    textFont(f,11);
    // x-coordinate label
    text(table.getColumnTitle(idx0), width/2, height-5 ); 
    
    // y-coordinate label (vertical text)
    float y_label1 = u0/2-10;
    float y_label2 = height/2;
    pushMatrix();
    translate(y_label1,y_label2);
    rotate(-HALF_PI);
    text(table.getColumnTitle(idx1), 0, 0);
    popMatrix();
    
    noFill();
    stroke(153);
    line( u0, v0-5, u0, h+v0); // y-axis line
    line( u0, h+v0, w+5, h+v0 ); // x-axis line
    
    for( int i = 0; i < 11; i++ ){    
      textFont(f,10);
      stroke(0);
      strokeWeight(0.5);
      // x-axis (vertical lines)
      line(u0 + (i * ((w-u0)/10)), v0+h-5, u0 + (i * ((w-u0)/10)), v0+h+5);
      
      // y-axis (horizontal lines)
      line(u0-5, v0+(i*(h/10)), u0+5, v0+(i*(h/10)) );
      
      // values to map x-y range tick marks
      float xValue = map(i, 0, 10, xMin, xMax);
      float yValue = map(i, 0, 10, yMax, yMin);
      
      // text is a little tricky to get aligned
      text(nf(yValue,idx0,idx1), u0/2-5, v0+5+(i*(h/10)));
      text(nf(xValue,idx0,idx1), u0-10 + (i * ((w-u0)/10)), v0+h+15);
    }
    
    for( int i = 0; i < table.getRowCount(); i++ ){
      float y = map( yList.get(i), yMin, yMax, v0+h, v0 );
      
      stroke(0);
      strokeWeight(0.7);
      fill(255);
      rect( u0 + (i * (w-u0)/table.getRowCount()), y, (w-u0)/table.getRowCount(), h+v0-y);
    }
  }
}